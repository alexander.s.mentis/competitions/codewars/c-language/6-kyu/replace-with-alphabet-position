#include <ctype.h>
#include <stdlib.h>
#include <string.h>

char * position(char ch) {
  
  int position = (int) toupper(ch) - (int) 'A' + 1;
    
  char *result = calloc(3, sizeof(char));
  sprintf(result, "%d", position);
  
  return result;
}

// returned string has to be dynamically allocated and will be freed by the caller
char * alphabet_position(const char *text) {
  
  int len = strlen(text);
  char *space = " ";
  
  // leave room for up to two digits and a space per char and the null terminator
  char *result = calloc(len*3 + 1, sizeof(char));
  
  for (const char *p = text; *p != '\0'; p++) {
    if (isalpha(*p)) {
      if (p != text) {
        strncat(result, space, 1);
      }
      strncat(result, position(*p), 2);
    }
  }
  
  return result;
}